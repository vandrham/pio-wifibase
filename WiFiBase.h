#ifndef wifi_h
#define wifi_h

class WiFiBase
{
    public:
        WiFiBase(const char *ssid);
        WiFiBase(const char *ssid, const char *password);
        void connect();
        bool connect(int timeout);
    private:
        const char *_ssid;
        const char *_password;
};

#endif