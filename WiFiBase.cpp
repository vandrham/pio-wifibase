#include "WiFiBase.h"
#include <Arduino.h>
#include <ESP8266WiFi.h>

WiFiBase::WiFiBase(const char *ssid)
{
    _ssid = ssid;
    _password = "";
}

WiFiBase::WiFiBase(const char *ssid, const char *password)
{
    _ssid = ssid;
    _password = password;
}

void WiFiBase::connect()
{
    connect(-1);
}

bool WiFiBase::connect(int timeout)
{
    if (WiFi.status() != WL_CONNECTED)
    {
        WiFi.mode(WIFI_STA);
        if (strlen(_password) == 0)
            WiFi.begin(_ssid);
        else
            WiFi.begin(_ssid, _password);
        
        while (WiFi.status() != WL_CONNECTED)
        {
            delay(1000);
            if (--timeout == 0) return false;
        }
    }

    return true;
}